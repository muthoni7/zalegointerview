$(document).ready(
    
    
    function(){      
      
        $("#regform").on('submit',(function(e) {
            e.preventDefault();
            $('.firstnameerror').html("");
            $('.lastnameerror').html("");
            $('.usernameerror').html("");
            $('.passworderror').html("");
            $('.cpasserror').html("");
            
            $.ajax({
                url: "regcon.php", 	// Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(res)   // A function to be called if request succeeds
                {
                    
                    if (res.firstname) {
                        $('.firstnameerror').html(res.firstname);
                    }

                    if (res.lastname) {
                        $('.lastnameerror').html(res.lastname);
                    }

                    if (res.username){    
                        $('.usernameerror').html(res.username);
                    }
                    if (res.password){
                        $('.passworderror').html(res.password);
                    }
                    if (res.cpass){
                        $('.cpasserror').html(res.cpass);
                    }
                  
                    if (res.Error){
                        swal("SORRY","REGISTRATION FAILED","error");
                    }
                    if (res.Success){
                            
                            
                        swal("REGISTRATION SUCCESSFULL","Please login to view details entered...","success",{
                            
                        });
                            
                        
                    }
                }
            });
        }));

        $("#loginpage").on('submit',(function(e) {
            e.preventDefault();
            
            $('.usernameerror').html("");
            $('.passworderror').html("");
            $.ajax({
                url: "../logincon.php", 	// Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(res)   // A function to be called if request succeeds
                {
                    
                   
                    if (res.username) {
                        
                        $('.usernameerror').html(res.username);
                    }
                    if (res.Password) {
                        $('.passworderror').html(res.Password);
                    }
                    if (res.Error) {
                        swal("SORRY","Account does not exist","error");
                    }
                    if (res.Success) {
                        swal(res.Success,"Please wait to be redirected...","success",{
                            buttons:false,
                        });
                        setTimeout("window.location.href='userspage.php'", 4000);
                        
                    }
                }
            });
        }));
        
    }
);
