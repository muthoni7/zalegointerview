
<html>
    <head>
        <title>SmartAcademy</title>
        
        <link rel="stylesheet" href="mycss/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="mycss/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="css/bootstrap4/js/bootstrap.min.js"></script>
        
        
    </head>
    <body>
    
   <?php include "profile.php"; ?>       
    
    <div class="container-fluid">
        <div class="row  mb-0 justify-content-center " style="">     
        
            <div id="homepg" class="col-md-12 pt-0 pl-0 " style="color: black;margin-top: 10px;">
                <div class="row "> 
                    <div class="col-md-8 mt-5 userprofile mr-auto ml-auto">
                        User Details
                    
                        <div class="col-md-12 mt-5" style="font-size:24;font-family:Times New Roman">

                        
                                <div class="row justify-content-center mt-3">
                                    <div class="col-md-2"><label>First Name</label></div>
                                        <div class="col-md-4">
                                        <?php echo $row['firstname'];?>
                                        </div>
                                </div>
                                <div class="row justify-content-center mt-3">
                                    <div class="col-md-2"><label>Last Name</label></div>
                                        <div class="col-md-4">
                                        <?php echo $row['lastname'];?>
                                    </div>
                                </div>
                                <div class="row justify-content-center mt-3">
                                    <div class="col-md-2"><label>Username</label></div>
                                        <div class="col-md-4">
                                        <?php echo $row['username'];?>
                                    </div>
                                </div>

                                
                            
                        </div>
                            
                            
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
       
    </div>
    </body>
    
</html>