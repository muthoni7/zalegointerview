<div id="registerpg" class="col-md-8 " >
    <div class='accountError alert alert-danger text-center' role='alert'> ALL FIELDS ARE MADATORY</div>
    <form id="regform" method="post">
        <h4 class="ml-5">Please Fill-In Your Personal Details Below</h4>
        
        

        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="firstname">First Name<span class="required">*</span> </label>
                <input class="form-control " type="text" id="firstname" name="firstname" placeholder="First name" />
                <span class="text-danger firstnameerror"></span>
            </div>
            <div class="col-md-6 mb-3">
                <label for="lastname">Last Name<span class="required">*</span> </label>
                <input class="form-control " type="text" id="lastname" name="lastname" placeholder="Last name" />
                <span class="text-danger lastnameerror"></span>
            </div>
            <div class="col-md-12 mb-3">
                <label for="username">Username <span class="required">*</span> </label>
                <input class="form-control " type="text" id="username" name="username" placeholder="Username" />
                <span class="text-danger usernameerror"></span>
            </div>
        
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password">PASSWORD <span class="required">*</span> </label>
                    <input class="form-control form-control-danger" type="password" id="password" name="password" placeholder="********"
                        onblur="checkvalue(this)" />
                        <span class="text-danger passworderror"></span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="cpass">CONFIRM PASSWORD <span class="required">*</span></label>
                    <input class="form-control form-control-danger" type="password" id="cpass" name="cpass" placeholder="********"/>
                    <br><span class="text-danger cpasserror"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <button class="btn btn-danger btn-md ml-0" type="reset" name="cancel">CANCEL</button>
            </div>
            <div class="col-md-6 text-right">
                <button class="btn btn-success btn-md " type="submit" name="submit"> SUBMIT</button>
            </div>
        </div>
    </form>
</div>