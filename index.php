
<html>
    <head>
        <title>SmartAcademy</title>
        
        <link rel="stylesheet" href="mycss/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="mycss/style.css">
        <link rel="stylesheet" href="mycss/fontawesome-free/css/all.css">
        <link rel="stylesheet" type="text/css" href="css/animate/animate.min.css"> 
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="css/bootstrap4/js/bootstrap.min.js"></script>
        <script src="js/sweetalert.min.js"></script> 
        
        <style>
            #loginpg{
                display: none;
            }
                #registerpg{
                display: none;
            }
            h4{
                color:saddlebrown;
                text-shadow: 2px 0 0 white;
                font-weight: bold;
            }
            label{
                color: peru;
                
                font-size: 15px;
            }
            .text-danger{
                font-size:15px;
            }
        </style>
    </head>
    <body>
    
           
    <nav class="navbar navbar-dark bg-primary navbar-expand-lg fixed-top">
        <a class="navbar-brand">
            Zalego Interview
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active mr-3 mt-2">
                </li>
                <li class="nav-item active mr-5">
                    <button class="nav-link btn btn-secondary btn-block  mb-1" id="home">
                        Home
                    </button>
                </li>
                <li class="nav-item active mr-5">
                    <button class="nav-link btn btn-secondary  btn-block   mb-1" id="login">
                        Login
                    </button>
                </li>
                <li class="nav-item active mr-5">
                    <button class="nav-link btn btn-secondary btn-block " id="register">
                        Register
                    </button>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row  mb-0 justify-content-center " style="margin-top: 60px;">     
        <div class="col-md-11  " id="section">
            <div id="homepg" class="mt-0 col-md-12 pt-0 pl-0 " style="color: black;">
                <div class="row mr-auto ml-auto"> 
                    <div class="col-md-8 mt-5 mr-auto ml-auto">
                        Zalego Interview - Home Page
                    
                        <div class="col-md-12 mt-5" style="font-size:24;font-family:Times New Roman">
                            
                            
                            Jackline Muthoni<br>
                            Laikipia University<br>
                            Year:2018<br>
                            Month: September<br>
                            
                            
                        </div>
                    </div>
                </div>
                
            </div>
            <div  class=" row justify-content-center mb-5">
                   <?php include "register.php"; 
                    include "login.php";?>
                </div>
                
            </div>
        </div>
       
    </div>
    </body>
    
    <script src="js/formscript.js"></script>
    <script src="js/formValidation.js"></script>
</html>