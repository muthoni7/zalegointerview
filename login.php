
<div id="loginpg" class="col-md-8 text-center" >
    <div class='accountError alert alert-danger' role='alert'> ALL FIELDS ARE MADATORY</div>
    <form action="" method="post" id="loginpage">
        <h4>Please Login Below</h4>
        <div class="row justify-content-center mt-3">
            <div class="col-md-2"><label for="username">Username <span class="required">*</span></label></div>
                <div class="col-md-4">
                    <input class="form-control " type="text" id="username" name="username" placeholder="username"  style="width:70%">
                    <br><span class="text-danger usernameerror"></span>
                </div>
        </div>

        <div class="row justify-content-center">
                <div class="col-md-2 mr-0"><label for="mypassword">PASSWORD <span class="required">*</span></label></div>
                <div class="col-md-4 ml-0">
                    <input class="form-control " type="password" id="mypassword" name="password" placeholder="*******" style="width:70%">
                    
                    <br><span class="text-danger passworderror"></span>
                </div>
        </div>

            <div class="row justify-content-center mt-1">
                <div class="col-md-4">
                    <button class="btn btn-danger btn-md" type="reset" name="cancel">CANCEL</button>
                </div>
                <div class="col-md-4 text-center">
                    <button class="btn btn-success btn-md" type="submit" name="submit">LOGIN</button>
                </div>
                    
            </div> 
    </form>
</div>